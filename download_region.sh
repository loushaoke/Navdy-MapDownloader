if [ ! -z "$CI_PROJECT_DIR" ]; then
  export PROJECT_DIR="$CI_PROJECT_DIR"
else
  export PROJECT_DIR="/root/tmp"
fi

pushd ${PROJECT_DIR}

REGION="$1"
DIR="$2"
mkdir -p zips
mkdir -p maps

if [ ! -f "./zips/${DIR}"_*.zip ]; then
  echo "$REGION -> $DIR"
  adb shell am instrument -w -e region "\"$REGION\"" -e mapsPath "\"/sdcard/$DIR\"" com.navdy.mapdownloader.test/android.support.test.runner.AndroidJUnitRunner
  echo "Copy maps from emulator..."
  [ ! -d $(dirname "./maps/${DIR}") ] && mkdir -p $(dirname "./maps/${DIR}")
  adb pull "/sdcard/${DIR}" "./maps/${DIR}" > /dev/null
  pushd "./maps/${DIR}"
  VERSION=$(cat ./.here-maps/version)
  ZIP= ${PROJECT_DIR}/zips/"${DIR}_${VERSION}.zip"
  echo "Zip maps to ${ZIP}"
  zip -q -r ${ZIP} .here-maps
  popd
  rm -rf "./maps/${DIR}"
  chmod 666 ${ZIP}
  adb shell rm -rf "/sdcard/${DIR}"

  if [[ $UPLOAD_GDRIVE -eq 1 ]]; then
    echo "Uploading ${ZIP} to gdrive"
    [ ! -d $(dirname "${HOME}/gdrive/navdy_maps/${DIR}") ] && mkdir -p $(dirname "${HOME}/gdrive/navdy_maps/${DIR}")
    cp -a "${ZIP}" $(dirname "${HOME}/gdrive/navdy_maps/${DIR}")
  fi

  if [[ ! -z "$UPLOAD_SCP" ]]; then
    echo "Uploading ${ZIP} to scp"
    [ ! -d $(dirname "${HOME}/navdy_maps/${DIR}") ] && mkdir -p $(dirname "${HOME}/navdy_maps/${DIR}")
    cp -a "${ZIP}" $(dirname "${HOME}/navdy_maps/${DIR}")
  fi

else
  echo "Exists: ./zips/${DIR}_*.zip"

fi
popd
